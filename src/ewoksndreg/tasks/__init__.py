"""Ewoks workflow tasks"""

from .example2d_stack import *  # noqa F401
from .reg2d_features import *  # noqa F401
from .reg2d_intensities import *  # noqa F401
from .reg2d_transform import *  # noqa F401
from .fluo_stack import *  # noqa F401
from .reg2d_multistack import *  # noqa F401
from .select_slice import *  # noqa F401
from .id16b_file_read import *  # noqa F401
