NAME = "Registration"

DESCRIPTION = "Data registration"

LONG_DESCRIPTION = "Data registration"

BACKGROUND = "light-blue"

WIDGET_HELP_PATH = (
    # Development documentation (make htmlhelp in ./doc)
    (
        "/home/sireiche/ewoksndreg/build/sphinx/html/index.html",
        None,
    ),
    # Documentation included in wheel
    # ("{}/help/ewoksndreg/index.html".format(sysconfig.get_path("data")), None),
    # Online documentation url
    ("https://ewoksndreg.readthedocs.io/en/latest/", ""),
)
