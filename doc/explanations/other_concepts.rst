Other Concepts
==============



Resampling
----------

In order to get the resulting image :math:`R` when applying a transformation :math:`T` to an image :math:`I`, we need values for points :math:`(x,y)\in\mathbb{N}^2`.
If we assume the original image is continuous, we could get these values as :math:`R(x,y) = I(T(x,y))`, but this is generally not possible as :math:`I` only has values at integer indices, and :math:`T(x,y)` could be any pair of real numbers.
To get values for non-integer positions, we need some way of interpolating the nearby values.

In practice, the main interpolation methods are nearest-neighbor, bilinear and bicubic interpolation.

.. list-table:: 

    * - .. figure:: img/resampling_nearest.png

           Fig 1. nearest-neighbor

      - .. figure:: img/resampling_bilinear.png

           Fig 2. bilinear
      - .. figure:: img/resampling_bicubic.png

           Fig 3. bicubic
