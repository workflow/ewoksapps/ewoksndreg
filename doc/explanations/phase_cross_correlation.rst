Fourier methods
===============


For translations, Fourier Transformations can be used to align images.
Based on the fourier shift theorem, we can generate an image, which has a peak at the coordinates corresponding to the shift between two images.
This allows us to find the translation with integer accuracy. There are ways of achieving subpixel registration, which allow for more exact calculation of the shift.

While we only get translations through the fourier shift theorem, it is possible to also register scale and rotation of images.
This is done through the log-polar transform, where shifts correspond to rotation and scaling in the original image.

Resources
---------

- `Wikipedia/Phase_Correlation <https://en.wikipedia.org/wiki/Phase_correlation>`_
- `ScikitImage Documentation <https://scikit-image.org/docs/stable/api/skimage.registration.html#skimage.registration.phase_cross_correlation>`_
- `SciKitImage Similarity with Cross Correlation <https://scikit-image.org/docs/stable/auto_examples/registration/plot_register_rotation.html>`_