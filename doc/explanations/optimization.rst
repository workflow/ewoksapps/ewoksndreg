Optimization-based Registration
===============================

When trying to find the transformation :math:`T` such that :math:`M \circ T` optimally matches :math:`F` for moving and fixed images :math:`M` and :math:`F`, 
we are required to define what the optimal match is. For feature-based approaches this would correspond to aligning matching features, 
but in order to phrase this as an optimization problem, we can define some distance 'metric' between two images, which we then can minimize.
Using this metric :math:`L` and the parameters :math:`\theta` defining the transformation :math:`T_{\theta}`, we can rephrase the task as

.. math::

   min_{\theta} L(M\circ T_{theta}, F)


In this form, we can just use an optimizer to find the optimal values of :math:`\theta`. This is the approach used by SimpleITK or Kornia.

Resources
---------

- `SimpleITK doc <https://simpleitk.readthedocs.io/en/latest/registrationOverview.html>`_