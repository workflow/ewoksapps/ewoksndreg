Installation
============

*ewoksndreg* provides functionality to register images using ewoks tasks and workflows.

The optimal way to use the tasks is through orange widgets, as visualizing the images is important to assess success of the alignment.

Step 1 - Environment
--------------------

We assume that your machine has python installed. You can either set up a new virtual environment to install this package in or use an existing one.
If you are using an existing environment you can skip the creation of the environment.

Go to the folder where you want to create the environment and create a python environment with (.venv an be replaced by a fitting name for the environment)

.. code-block:: bash

    python -m venv .venv

you can then activate the virtual environment with

.. code-block:: bash

    source .venv/bin/activate

This setup is valid for Unix/macOS.
For a more thorough walkthrough and the windows version, see the `python guide <https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment>`_

Step 2 - Package
----------------

We can use `pip <https://packaging.python.org/en/latest/tutorials/installing-packages/>`_ to install the *ewoksndreg* package with

.. code-block:: bash

    pip install ewoksndreg[full]

The environment must be activated for this, which is indicated by the `(venv)` in the beginning of the line in the terminal

This will install the package and all its dependencies. If you have issues with memory space and don't need all options for backends, 
an alternative would be to only install the following packages as this leaves out Kornia and its dependency pytorch which are fairly big.

.. code-block:: bash

    pip install ewoksndreg
    pip install simpleitk
    pip install scikit-image



Step 3 - First workflow
-----------------------

It should now be possible to use the tasks in ewoks workflows.  We can now launch the Orange GUI with 

.. code-block:: bash

    ewoks-canvas

In Orange we can first execute an example workflow, to see if everything works correctly. 
The window with example workflows can be found through the example tab in the 'Welcome to Orange' Window or through \>  Help \> Example Workflows.
You can open either of the workflows 'Feature-based Image Registration' and 'Intensity-based Image Registration'. 

To trigger the workflow you can open the '2D Example Stack' Task and press 'Trigger'. After a short computation, you should find the aligned images in the 'Align' task


Step 4 - Next steps
-------------------

For more information about the specific widgets and underlying tasks, see :doc:`/widgets`.

A small introduction and some links for further reading regarding the theory can be found in :doc:`/explanations`.



