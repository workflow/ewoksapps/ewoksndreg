Theory
======

This section provides some introductory explanations of the concepts involved in image registration. 

.. toctree::
    :maxdepth: 2

    /explanations/features.rst
    /explanations/phase_cross_correlation.rst
    /explanations/optimization.rst
    /explanations/other_concepts.rst