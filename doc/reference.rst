Reference
=========

Page on the choices and parameters which provides tips on how to select the parameters if issues with the registration arise:

.. toctree::
  :maxdepth: 1

  reference/correct_parameters.rst



.. toctree::
    :maxdepth: 2

    reference/api