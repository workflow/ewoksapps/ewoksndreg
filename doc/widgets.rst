Available Widgets
=================



.. toctree::
    :maxdepth: 1

    reference/widgets/2D_intensity-based_registration.rst
    reference/widgets/2D_feature-based_registration.rst
    reference/widgets/2D_transformation.rst
    reference/widgets/Pre-registration_evaluation.rst
    reference/widgets/Post-registration_evaluation.rst
    reference/widgets/2D_example_stack.rst