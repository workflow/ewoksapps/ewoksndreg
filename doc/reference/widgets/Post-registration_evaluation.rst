Post-Registration Evaluation
============================

Find the best list of transformations given multiple lists and the images they were registered from

.. image:: img/2D_posteval.png

Parameters
----------
imagestacks:
    A list of imagestacks that have been used as a basis for alignment
transformations:
    The resulting transformations for each of the imagestacks
url:
    Url of an hdf5 Dataset to save the chosen transformed imagestack
inputs_are_stacks:
    If true: Will treat imagestacks input of shape [B,N,M,H,W] as [B*N,M,H,W]
chosen_stack:
    The index of the stack with the alignment of choice

Notes
-----

Deciding whether an alignment is "good" is very hard to solve algorithmically, therefore the attemps to find the best alignment in this task
are in no way definitive.

For every imagestack there are two measures used:
    - Mean squared error between the images
    - Smoothness of the transformations using change of corner coordinates when applying successive transformations

