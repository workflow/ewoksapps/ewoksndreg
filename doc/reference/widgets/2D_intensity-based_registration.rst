2D Intensity-Based Registration
===============================

Register images to a reference image based on intensity-based methods

.. image:: img/2D_intensity-based_registration.png


Parameters
----------

imagestack
    One or multiple stacks of images as list of arraylikes or 
mapper
    The method used for aligning the images. Options: ("CrossCorrelation-Numpy",
    "CrossCorrelation-SciKitImage", "Optimization-SimpleITK", "Optimization-Kornia")
transformation_type
    Type of the transformation the method should register for. The options are dependent on the 
    mapper and are given in :py:const:`~ewoksndreg.intensities.base.IntensityMapping.SUPPORTED_TRANSFORMATIONS` for the chosen mapper
mask
    Section of the images to use for the registration. Should be specified as a binary image with 1 corresponding to pixels used for the registration.
    In the widget, this input can be specified using the mask tool of the plot. Masks are only supported by certain mappers, see :ref:`method`
inputs_are_stacks
    If true: Will treat imagestack input of shape [B,N,H,W] as [B*N,H,W]
block_size
    Strategy to generate all transformations to reference based on pairwise registration. For explanation see :ref:`block_size`
reference
    The index of the reference image. The calculated transformations will be relative to this image.
preprocessing_options
    Filters, windows and other operations that will be applied to the image before calculating the transformation. See :ref:`preproc`
method_options
    Additional method-dependant parameters. See :ref:`method`

.. _block_size:

Block Size
----------

The block_size parameter is best understood with the following image, where squares represent the images and the arrows the calculated transformation.
The full transformations from any image to the reference is then generated through composition of the registration results. 
There is a tradeoff between amount of compositions (which can accumulate error) or 'distance' of registration (which decreases the registration success).
As the option of including only a subset of the images is not possible yet, the fourth case is currently irrelevant.

.. image::
    img/block_size.png


.. _preproc:

Preprocessing Options
---------------------

There are several operations that can be applied to the image in the order:

1. Apply filter (Sobel, Sato,...)
2. Low-,High-,Band-pass the image
3. Pin range
4. Apply window (Hann, Hamming,...)

Filtering(``apply_filter`` and ``filter_parameter``):
    In order to highlight certain image features like edges, ridges, veins or other aspects, apply one of the filters from :py:class:`~ewoksndreg.math.filter.FilterType`
    For more details see the `ScikitImage API <https://scikit-image.org/docs/stable/api/skimage.filters.html>`_. 
    For Sato and Meijering, another parameter can be specified though the additional value


Band-passing(``apply_low_pass`` and ``apply_high_pass``):
    Limit the frequencies in the image by applying a band-pass filter. This is done by gaussian smoothing of the image with two different sigmas.
    The image with the higher sigma is then subtracted from the image with the lower sigma. See `difference_of_gaussians() <https://scikit-image.org/docs/stable/api/skimage.filters.html#skimage.filters.difference_of_gaussians>`_. 

Range pinning(``pin_range``):
    Normalize the image such that the maximum intensity of the image is 1 and the minimum is 0

Windowing(``apply_window``):
    Apply a window function to the image to remove border effects. For more details: `Scipy API <https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.windows.get_window.html>`_.
    The possible windows are specified in :py:class:`~ewoksndreg.math.filter.WindowType`

.. _method:

Methods
--------------

CrossCorrelation-Numpy
    - Supported Transformations: Translation
    - No additional Parameters
    - No support for masks

CrossCorrelation-SciKitImage
    - Supported Transformations: Translation, Similarity
    - Parameters:
        :normalization: Should a normalized or unnormalized version of the phase-cross-correlation be computed. 
        :upsample_factor: Images will be registered to within ``1 / upsample_factor`` of a pixel
    
    - If a mask is provided, both parameters will be ignored
    - See `phase_cross_correlation() <https://scikit-image.org/docs/stable/api/skimage.registration.html#skimage.registration.phase_cross_correlation>`_

Optimization-Kornia
    - Supported Transformations: Translation, Similarity, Projective
    - Parameters:
        :metric: The metric used to determine the similarity of the images
        :optimizer: The optimizer used to minimize the metric
        :pyramid_levels: Amount of layers in the image pyramid 

    - No support for masks
    - See `Kornia documentation <https://kornia.readthedocs.io/en/latest/applications/image_registration.html>`_

Optimization-SimpleITK
    - Supported Transformations: Translation, Rigid, Similarity, Affine, BSpline, Displacement Field
    - Parameters:
        :metric: The metric used to determine the similarity of the images
        :optimizer: The optimizer used to minimize the metric
        :pyramid_levels: Amount of layers in the image pyramid 
        :order: Order of interpolation used when resampling the image during optimization
        :sampling: Strategy for choosing points in the images to calculate the metric. Either None, "random" or "regular"
        :sampling%: If sampling strategy is random or regular: What percentage of the image should be used to compute the metric

    - Supports masks
    - See `SimpleITK documentation <https://simpleitk.org>`_

.. _visual:

Visualizations
--------------

There are several different representations of the image shown in the widget. To access these use the tabs in the main area of the widgets

images
    Shows the images after the preprocessing has been applied. This plot also allows for specifing the mask

fourier
    The fourier transform of the preprocessed image. This can be used to inspect artifacts in the image, which cause issues mainly for phase cross-correlation.
    One source of registration error stems from the edges of the images. These errors are indicated in the fourier transform through the big "plus"-like shape in the image:

    .. image:: img/cross.png
        :height: 300px

    Applying high-pass filters or window functions removes these errors, so that a better fourier transform might look like this:

    .. image:: img/no_cross.png
        :height: 300px

cross_correlogram
    The cross-correlogram between the chosen image and the reference image. This image is the result of the phase cross correlation algorithm 
    and contains the translation as the index of the highest peak in the image. We can use this to view how much noise there is and how definite the peak is.
    One bad example would be

    .. image:: img/bad_correlogram.png
        :height: 300px

    and after applying some filtering and windowing we can achieve a better result on this image pair

    .. image:: img/good_correlogram.png
        :height: 300px









