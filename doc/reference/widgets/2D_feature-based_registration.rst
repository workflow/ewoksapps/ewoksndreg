2D Feature-Based Registration
=============================

Register images to a reference image based on feature-based methods

.. image:: img/2D_feature-based_registration.png

Parameters
----------

imagestack
    A stack of images (3D Arraylike)
detector
    Method to detect and describe feature points in an image. Options: ('Sift-Silx', 'Sift-SciKitImage', 'Orb-SciKitImage')
matcher
    Method to build correspondence between the feature points in two images. Options: ('Descriptor-Silx', 'Descriptor-SciKitImage')
mapper
    Method to find parameters of the transformation to best satisfy the matches Options: ('LstSq-Numpy', 'LstSq-SciPy', 'Ransac-SciKitImage')
transformation_type
    Type of transformation the mapper uses to satisfy the matches
inputs_are_stacks
    If true: Will treat imagestack input of shape [B,N,H,W] as [B*N,H,W]
reference  
    The image in the stack that everything will be aligned to