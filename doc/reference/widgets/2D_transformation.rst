2D Transformation
=================

Apply geometric transformations to images

.. image:: img/2D_transformation.png


Parameters
----------

imagestack
    One or multiple stacks of images as list of arraylikes

transformations
    One or multiple lists of transformations that are either represented by Transformations or 3x3 matrices

url
    Location to and in .h5-file where the resulting images/stacks will be saved as a dataset (one stack) or a group containing datasets (multiple stacks)

inputs_are_stacks
    If true: Will treat imagestack input of shape [B,N,H,W] as [B*N,H,W]

crop
    Only for translations: Crops the resulting images to remove all NaN-values

interpolation_order:
    Determines the order of interpolation used for resampling the image. See :doc:`/explanations/other_concepts`

stack_names:
    If several stacks and their names are provided, the resulting images will be saved using these names