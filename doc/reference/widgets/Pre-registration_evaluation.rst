Pre-Registration Evaluation
===========================

Orders the given stacks by likelihood of yielding a successful alignment when using phase-cross-correlation to align by translation

.. image:: img/2D_preeval.png

Parameters
----------

imagestacks
    List of imagestacks that require the same alignment
inputs_are_stacks
    If true: Will treat imagestacks input of shape [B,N,M,H,W] as [B*N,M,H,W]
stack_names
    Names of the different stacks
preferences
    List of indices or names (if provided) that will take precedence over the calculated ranking
relevant_stacks
    Amount of stacks that will be returned

Outputs
-------

full_ranking
    All indices in the calculated order
ranking
    Identical to full_ranking, but truncated to {relevant_stacks} elements if specified
stack_names
    The names of the first {relevant_stacks} stacks, if stack_names have been specified

Notes
-----

This ranking is only relevant for images requiring translations and does not have meaningful output in other cases

The ranking is based on the phase-cross-correlation algorithm. 
The cross-correlogram between the first and every other image is calculated, which has a peak corresponding to the shift between the images.
By taking the ratio of the peak value and the mean, we can examine how sure this method would be and how much the two images correspond to a pure translation