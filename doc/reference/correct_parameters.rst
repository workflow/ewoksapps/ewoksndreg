Reference for choosing the right parameters
===========================================


Intensity-based vs Feature-based
--------------------------------
Feature-based methods have higher requirements to be applicable:
    - Mainly used on medium to big images \>( 100, 100)
    - Requires the existance of features in the image, these can be corners, blobs and similar "interesting" points
    - Fails if there are big changes in the intensities in the area around the features
    - There should be no ambiguity in the feature locations, so no gradual edges or corners
    - Subpixel registration only partially possible

However, there are advantages to aligning based on points of interest and not on the entire image, 
as the correspondences between the images generated through the features are more robust than the correspondences between pixels in intensity-based methods.

.. _preprocessing:

Preprocessing
-------------

Filters:
    - Gaussian Filter: Can mitigate Gaussian noise
    - Median Filter: Can mitigiate salt-and-pepper noise

    - Sobel, Sato, Meijering: Used to amplify edges, ridges, veins and other similar structures

Bandpass:
    - low_pass: Cuts off high frequencies, similar to gaussian Filter
    - high_pass: Cuts of low frequencies, often useful to improve phase cross correlation success

Range:
    - Pin range: Scales image to range [0,1] as most frameworks assume intensities in this range. Can have negative impact on registration

Window:
    - Different windows have different effects on attenuation of different frequencies, hard to know which are good.
    - If using a fourier based method (phase cross correlation): Look at fourier transform and Cross-Correlogram to see if the window is good. (No big plus in fourier transforms and a definite peak in the cross Cross-Correlogram)


Strategy
--------

Strategy to get all transformations from reference to any image using pairwise registration. Consists of `reference` and `block_size`.

reference:
    - Index of an image in the stack. The returned transformations will align all images to the reference image
    - If ``reference == -1``, the reference image will be an image in the middle of the stack
    - This choice has an impact on the results, as big changes over several images can be halfed if the reference is the central image.

block_size:
    - Defines the method used to calculate all transformations to the reference.
    - If there are big changes in the intensities or big gradual geometric transformations, small block_size is recommended
    - As displacement fields compose very badly, a block_size bigger than the amount of images should be used
    - Due to inaccuracies for phase cross correlation, many compositions should be avoided -\> bigger block_size


Optimizer vs Phase Cross correlation(PCC) for translations
----------------------------------------------------------

No definite answer which method is better in what situation, but:
    - PCC usually has a better runtime
    - For individual pairwise registration, PCC deals better with big transformations if there is no good initial guess, as optimization starts far away from the minimum
    - It is easier to inspect PCC when trying to find good filters or windows to apply to the image
  
    - Optimizer can get better subpixel registration
    - More options with different metrics

Method Options
--------------

`SciKit-Image PCC <https://scikit-image.org/docs/stable/api/skimage.registration.html#skimage.registration.phase_cross_correlation>`_ :
    - Normalized vs Unnormalized: Application-dependant, for high noise unnormalized is preferable, for differing illumination normalized is preferable
    - Upsample Factor: Can be set depending on image size, for image sizes over (1000, 1000) a factor of 5 is probably enough, for small images a high value (\~100) might be required to remove any jitter

    - Keep in mind: When using masks, both parameters are ignored

`SimpleITK Optimization <https://simpleitk.readthedocs.io/en/latest/registrationOverview.html>`_:
    - Metric: 
        * Mean Squares, Cross Correlation and Neighborhood Correlation: General purpose metrics for defining image similarity
        * Both versions of Mutual Information: Provide some some invariance to changes in intensities, but have a worse runtime
    - Optimizer:
        * l-bfgs-b is good base optimizer 
        * Variants of Gradient Descent can be used if l-bfgs-b gives bad results or is not available
        * Exhaustive can provide initial transforms in case of big transformations where local optimization is not valid
    
    - Order: Should be bigger for small images, for big images 1 should be enough, small images might take 3
    - Pyramid_levels: Only relevant if there are big transformations between registered images. In these cases 3 layers are probably more than enough
    - Sampling: Only relevant for runtime considerations

`Kornia Optimization <https://kornia.readthedocs.io/en/latest/applications/image_registration.html>`_:
    - Metric:
        * Mean-Squared and Mean-Absolute Error: General Purpose image similarity
        * NMI: Variant of Mutual Information. Has inefficient implementation and should only be used for few and small images
    - Optimizer: RMSProp should be first choice
    - Pyramid_levels: Same as SimpleITK