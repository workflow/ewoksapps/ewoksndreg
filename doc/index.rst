ewoksndreg |version|
====================

*ewoksndreg* provides workflows for data registration (e.g. curve, image and volume registration).

*ewoksndreg* has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_ of the `European Synchrotron <https://www.esrf.fr/>`_.


.. image:: img/workflow.png


First Steps
-----------

.. toctree::
   :maxdepth: 1
   
   how-to_guides/installation.rst


Tutorials
---------

Simple Walkthroughs of some possible workflows for registration:

.. toctree::
  :maxdepth: 2

  tutorials.rst


.. _widgets:

Widgets
-------
.. the orange widgets section has to be stored here to fit the orange requirement (and accessing the help from orange-canvas)

List of main Orange widget provided in the package:

.. toctree::
  :maxdepth: 2
  
  widgets


Reference
---------

API reference and page for tips on picking the right parameters:


.. toctree::
  :maxdepth: 2

  reference.rst



Explanations
------------

Theory behind the algorithms used for alignment and adjacent topics:

.. toctree::
    :maxdepth: 2

    explanations.rst

