Denoising with filters
======================

We will look at the issue of noise when trying to align images and how to edit the images to improve the registration.

For this we will use the example workflow reg2d_workflow_noise:

.. image:: imgs/noise_workflow.png

We generate some example data which requires translations to be aligned in the first task and create several different copies of this stack with different levels of salt-and-pepper noise.
Some of our input data might look like this:

|pic1| |pic2|

.. |pic1| image:: imgs/noisy_image1.png
   :width: 45%

.. |pic2| image:: imgs/noisy_image2.png
   :width: 45%

For some of the created stacks, alignment will not be a problem, while others will fail with most algorithms, as for some images about half the pixels have been set to 0 or 1.
This level of noise really extreme compared to real examples, but exemplifies the issue. 
After the registration we are able to pick the stack and its transformations that correspond to the best alignment. In this example the chosen stack will most likely be one with little to no noise,
however in most instances we don't have the noiseless images. In order to improve the registration result on the more noisy stacks, we can use the options in the preprocessing tab.

.. image:: imgs/preprocessing_tab.png


There are two main options which provide denoising for different types of noise. The gaussian filter is useful in cases of gaussian noise, while the median filter 
provides denoising for salt-and-pepper noise or similar types (:ref:`preprocessing` ). 
When inspecting the images in the registration task, we can already see a major improvement in the image quality. 
As we are interested in translations, the cross-correlogram (:ref:`visual`) provides similar insights, as we can see a more distinctive peak when applying the median filter.

|pic3| |pic4|

.. |pic3| image:: imgs/noisy_image1_med.png
   :width: 45%

.. |pic4| image:: imgs/noisy_image2_med.png
   :width: 45%

In this instance the denoising allowed us to achieve proper registration with higher noise. 
In practice, noise is not as ideal as in this example, but the filters for denoising might still be able to provide some improvement.