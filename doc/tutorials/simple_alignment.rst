How to align one stack of images
================================

The simplest workflow takes one of the forms 

|pic1| or |pic2|

.. |pic1| image:: imgs/simple_feat_reg_workflow.png
   :width: 45%

.. |pic2| image:: imgs/simple_int_reg_workflow.png
   :width: 45%


The three tasks are:
  1. Fetching of the input data. This can be done either through a dedicated task where the task loads the images or, in case of one or multiple h5 datasets that contain the images, as direct input to the registration task using the data locations. 
  2. Performing the registration. There are two different tasks for this, which are based on either feature-based or intensity-based methods. What kind of method works best is problem dependent and deserves its own discussion.
  3. Applying the calculated transformations to the images. 


