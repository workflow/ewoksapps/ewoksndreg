# CHANGELOG.md

## Unreleased

## 0.3.0

Changes:

- Drop support for Python 3.7
- Support numpy v2

## 0.2.0

New features:

- Add support for Python 3.12

## 0.1.1

Bug fixes

- Convert enumeration fields to strings
- `pyopencl` upper bound because of sqlite3 threading errors

## 0.1.0

First release. The package provides tasks to do:

- Intensity based registration
  - Methods: cross-correlation, optimization
  - Backends: numpy, SimpleITK, scikit-image, kornia
- Feature based registration:
  - Detection: SIFT, ORB, CENSURE, HARRIS
  - Matching: LSTSQ, RANSAC
  - Backends: numpy, scikit-image, scipy
