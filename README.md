# ewoksndreg

Workflows for data registration (e.g. curve, image and volume registration).


## Installation

``` python
pip install ewoksndreg
```

``` python
pip install ewoksndreg[full]
```

## Documentation

https://ewoksndreg.readthedocs.io/
